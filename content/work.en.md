+++
weight = 20
draft = false
date = "2017-03-02T11:59:05-05:00"
title = "Work"

+++

{{< figure class="image main" src="/images/pic02.jpg" >}}

* [Flow](https://web.flow.com.ar/)
* [Qubit.tv](https://www.qubit.tv/)
* [Basso Brovelli](http://www.bassobrovelli.com/)
* Ambassador Fueguina
* [Studio Patagonia](http://www.studiopatagonia.com.ar/)
* [Crystal Solutions](http://www.crysol.net/)

## CV
* [pdf](/russitto.pdf)
* [txt](/russitto.txt)
