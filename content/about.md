+++
title = "Acerca"
weight = 30
draft = false
+++

{{< figure class="image main" src="/images/pic03.jpg" >}}
Mi nombre es Matias, tengo 40 años.

Soy de Buenos Aires, Argentina. Pero viví 5 años en Ushuaia. Un lugar extraordinario.

Me gustan las pelis, las series y escuchar música.
