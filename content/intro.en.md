+++
weight = 10
date = "2017-03-02T10:54:38-05:00"
title = "Intro"
draft = false

+++

{{< figure class="image main" src="/images/pic01.jpg" >}}

## Matias Russitto


### Node.js Programmer

* +5 years
* Fastify, Express.js, Restify
* Vainilla ES6, CoffeeScript

### JavaScript Programmer

* +10 years
* Vainilla ES5/ES6, lodash, webpack

### PHP Programmer

* +15 years
* Yii, Symfony & others

[My Works](#work)
