+++
title = "Intro"
weight = 10
draft = false
+++

{{< figure class="image main" src="/images/pic01.jpg" >}}

## Matias Russitto


### Programador Node.js

* +5 años de profesión
* Fastify, Express.js, Restify
* Vainilla ES6, CoffeeScript

### Programador JavaScript

* +10 años de profesión
* Vainilla ES5/ES6, lodash, webpack

### Programador PHP

* +15 años de profesión
* Yii, Symfony y otros

[Mis trabajos](#work)
