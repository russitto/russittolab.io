+++
title = "Contacto"
weight = 40
draft = false
+++

<form id="contactform" method="post" action="https://formspree.io/matias@russitto.com">
	<div class="field half first">
		<label for="name">Nombre</label>
		<input type="text" name="name" id="name" />
	</div>
	<div class="field half">
		<label for="_replyto">Email</label>
		<input type="text" name="_replyto" id="_replyto" />
	</div>
	<div class="field">
		<label for="message">Mensaje</label>
		<textarea name="message" id="message" rows="4"></textarea>
	</div>
	<ul class="actions">
		<li><input type="submit" value="Enviar" class="special" /></li>
		<li><input type="reset" value="Resetear" /></li>
	</ul>
</form>

<span id="contactformsent">Gracias!</span>

<script>
$(document).ready(function($) { 
    $(function(){
        if (window.location.search == "?sent") {
        	$('#contactform').hide();
        	$('#contactformsent').show();
        } else {
        	$('#contactformsent').hide();
        }
    });
});
</script>


{{< socialLinks >}}
