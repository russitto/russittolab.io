.. -*- coding: utf-8 -*-

===============
Matias Russitto
===============

.. |date| date:: %d/%m/%Y %H:%M
.. |version| date:: %Y%m%d
.. contents:: Contenidos

.. header::

  ###Title### - matias@russitto.com - Programador

.. footer::

  ###Page### / ###Total###

.. .. raw:: pdf
.. 
..    PageBreak

------------
Hoja de vida
------------

Personal
========

  Nombre
    Matias Russitto

  D.N.I.
    27279615

  Fecha de nacimiento
    17 de Abril de 1979

  Edad
    41 años

  Dirección
    Coronel Forest 1978, Aldo Bonzi

  Teléfono
    Móvil: +549 11 2593 4388

  E-mail
    matias@russitto.com

  Sitio
    https://gitlab.com/russitto

.. raw:: pdf

  PageBreak

Educación
=========

  2020
    Curso Red Hat OpenShift Developemnt I

  2016
    Curso Node.js + MongoDB en Educación IT

  2016
    Concurrencia a Nodeconf Argentina

  2015
    Seminario Testing de Seguridad en Aplicaciones Web (OWASP)

  2015
    Curso Experto en JavaScript Avanzado

  2010 a 12-2011
     Universitaria: Licenciatura en Sistemas (Universidad San Juan Bosco).

  2006
     Seminario Eclipse / IBM: IBM Technical Breafings: Eclipse - IBM Software
     Development Plataform (Universidad de Palermo).

  2005
     Curso Java J2SE: Programación Java (SL-210 Y SL-275) (Universidad
     Tecnológica Nacional).

  2003 a 2004
     Universitaria: Profesorado en Ciencias de la Computación (materias de
     1er año, UBA).

  1998 a 2004
     Universitaria: Licenciatura en Ciencias de la Computación (materias de
     3er año, UBA).

Conocimientos
=============

  Web
     Node.js, Express, PHP, CakePHP, Yii, Symfony, WordPress, ASP, JSP, JavaScript, Ajax, JSON, XML, XSL, XSLT, XHTML, CSS.

  Business Intelligence
     BusinessObjects Web Intelligence,Business Objects Enterprise, Crystal Reports.

  Bases de datos
     MongoDB, Datawarehousing, MsSql, Ms Analysis Services, Ms DTS (Etl), Access, BusinessObjects Data, Integrator, Oracle, Teradata, MySql, Postgresql.

  Sistemas operativos
     GNU/Linux, Windows, Freebsd.

  Dictado de cursos
     Business Objects Enterprise, Crystal Reports, Php, Ajax, GNU/Linux, Windows, Office. Html básico en taller de la Universidad San Juan Bosco.

Experiencia
===========

  06-2018 a Actualidad
     Líder técnico backend Node.js en Telecom, proyecto Flow

  04-2014 a 06-2018
     Líder técnico, desarrollador senior PHP + JS + Node.js, scrum master en http://www.qubit.tv/

  04-2012 a 04-2014
     Programación web en http://www.bassobrovelli.com/ (PHP, CakePHP, Yii, MySQL, HTML, CSS, JavaScript, Ajax, bash, sh, GNU/Linux, ssh)

     Clientes: Puma, Casancrem, Bodegas Lagarde, Branca, Villa del Sur, ...

  05-2011 a 12-2011
     Administrador de sistemas y desarrollador en relación de dependencia en fábrica de TV Ambassador Fueguina SA (administración SQL Server, desarrollos en PHP)

  07-2008 a 02-2010
     Dictado de cursos en forma independiente (Windows, Office, Linux, Programación)

  05-2008 a 12-2008
     Idea y Desarrolllo de microblog opensource http://www.agendami.com

  09-2007 a 11-2011
     Analista Senior Web en cliente, Diseñador Funcional Web, Desarrollador Web en http://www.studiopatagonia.com.ar

  04-2007 a 09-2007
     Analista Senior Web en relación de dependencia, Diseñador Funcional Web, Desarrollador Web en GuiarTDF

  10-2004 a 01-2007
     Analista Senior BI, Diseñador funcional, Desarrollador y soporte en Crystal Solutions en relación de dependencia (http://www.crystalsolutions.com.ar)

  08-2003 a 10-2003
     Programación en relación de dependencia, diseño y mantenimiento de los sitios de Editorial Argenta http://www.editorialargenta.com, http://www.newsbookonline.com, http://www.marketaxa.com y http://www.storyline.ws en ASP - SQL - HTML - JavaScript

Freelance / Independiente
-------------------------

  01-2012 a 04-2012
     Programación web freelance en http://h1lab.com/ (PHP, CakePHP, MySQL, HTML, CSS, JavaScript, Ajax, bash, sh, GNU/Linux, ssh)

  10-2010 a 05-2011
     Desarrollador web freelance en http://altodot.com/ (PHP, MySQL, HTML, CSS, JavaScript, Ajax, bash, sh, GNU/Linux, ssh)

  07-2010 a 09-2010
     Desarrollador web freelance en http://p3design.com/ (PHP, Yii, MySQL, HTML, CSS, JavaScript, Ajax, bash, sh, GNU/Linux, ssh)

  09-2009 a 06-2010
     Programación freelance del sitio http://www.futbolinker.com/ en conjunto con el equipo de http://h1lab.com/ (PHP, CakePHP, MySQL, HTML, CSS, JavaScript, Ajax, bash, sh, GNU/Linux, ssh)

  08-2009 a 09-2009
     Programación freelance del sitio http://www.wantmoreunilever.com/ en conjunto con el equipo de http://h1lab.com/

  06-2009 a 08-2009
     Programación freelance del sitio http://www.armatuequipo.com/ en conjunto con el equipo de http://h1lab.com/

  01-2008 a 06-2008
     Analista Senior Web freelance, Diseñador Funcional Web, Desarrollador Web de http://www.beleeble.com para http://www.enclave.com.ar

  12-2007 a 01-2008
     Analista Senior Web freelance, Diseñador Funcional Web, Desarrollador Web en KS Solutions

  07-2004 a 10-2004
     Desarrollo freelance para Cubik del sitio de Telespazio, en PHP-Mysql-Flash

  01-2004 a 09-2004
     Desarrollo freelance en SysProgress del Sistema de Gestión y Distribuidores de Forever Living Argentina, en PHP - Mysql - Html - JavaScript

  10-2003 a 01-2004
     Freelance de programación, diseño y mantenimiento de los sitios de Editiorial Argenta

  07-2002 a 08-2002
     Freelance para Cubik, desarrollo de backend en PHP-MySQL, para sistema de administracion de mailserver de la Universidad de Bologna representación Buenos Aires

.. .. raw:: pdf
.. 
..   PageBreak

Proyectos
=========

Consultoría BI
--------------

  Coca - Cola
     desarrollo de reportes y aplicación web de Business Intelligence

  Prosegur
     desarrollo de aplicación web y reportes de Business Intelligence

  Orígenes
     desarrollo de reportes de Business Intelligence

  Bimbo
     desarrollo de aplicación web de Business Intelligence

  Cencosud Chile
     Supermercados Jumbo, desarrollo de reportes de Business Intelligence (ciudad: Santiago de Chile)

  McCain
     desarrollo de reportes de Business Intelligence

  Teletech
     desarrollo de reportes de Business Intelligence

  Argencard
     mantenimiento de plataforma de Business Intelligence

  Siemens
     consultoría de Business Intelligence

  Telmex
     consultoría de reportes de Business Intelligence

  Crystal Planning
     desarrollo de aplicación web de planeamiento de presupuestos para clientes (Coca - Cola, Sancor, La Virginia, ...)

  Sancor Seguros
     desarrollo de aplicación web de Business Intelligence

  Business Intelligence
     Dictado de cursos de herramienta de reportes, y aplicación web

Investigación personal
----------------------

  09-2009 a 05-2010
     Desarrollo de livecd dwmarch (Arch Linux + dwm) (offline) http://www.sarckz.com.ar/totoloco/

  09-2009 a 09-2009
     Desarrollo de plugin Vim para Pasto http://www.vim.org/scripts/script.php?script_id=2798

  08-2009 a 02-2010
     Idea, desarrollo y manenimiento de Planeta TDF (noticias de la Tierra del Fuego) http://www.planetatdf.com.ar/

  08-2009 a 02-2010
     Idea, desarrollo y manenimiento de simpl@net (motor de Planeta TDF) http://trac-hg.assembla.com/simplanet

  03-2009 a 04-2009
     Desarrollo de plugin WordPress Atpic para insertar galería de fotos

  03-2009 a 04-2009
     Desarrollo de plugin WordPress TryMath como reemplazo de imágenes captcha

  02-2009 a 11-2009
     Desarrollo de AnpWui http://trac-hg.assembla.com/net-anp, interfaz web para net-anp, monitor descentralizado de red 

  01-2009 a 02-2010 Idea y Desarrollo de pastebin opensource http://pasto.sourceforge.net

  10-2008 a Actualidad
     Desarrollo de paquetes unsupported para Archlinux

  09-2008 a 09-2008
     Desarrollo de widget para http://www.agendami.com para navegador Opera

  08-2008 a Actualidad
     Idea y Desarrollo de http://ushcompu.com.ar

  12-2007 a 02-2008
     Idea y Desarrollo de http://www.geobyte.com.ar

  09-2007 a 10-2007
     Idea y Desarrollo de http://www.russitto.com.ar

  2002 a 2006
     Desarrollo, programación, diseño y mantenimiento del sitio http://www.enlavia.8k.com (fanzine web) en ASP - SQL - HTML - Flash

  2002 a 2006
     Desarrollo, programación, diseño y mantenimiento del sitio http://www.sirvelocet.com.ar (SVP SirVelocet Producciones) en HTML - JavaScript

  1999 a 2002
     Idea, desarrollo, programación, diseño y mantenimiento de los sitios http://www.sazazza.com.ar (offline) en HTML

Web
---

 - https://web.flow.com.ar/
 - https://russitto.com/
 - https://www.qubit.tv/
 - http://casanhelp.com/
 - http://www.planetatdf.com.ar/
 - http://www.agendami.com/
 - http://www.beleeble.com/
 - http://www.vortilon.com/
 - http://www.vortilon.com/figaro
 - http://www.tdmoc.com.ar/admin
 - http://www.ushuaiaalojamientos.com.ar/
 - http://www.telespazio.com.ar/
 - http://www.lapaginaexpress.com.ar/

.. .. raw:: pdf
.. 
..   PageBreak

Open Source
-----------

 - https://gitlab.com/russitto

Información extra
=================

.. contents

:Date: |date|
:Version: |version|.2
:Author: Matias Russitto
:Contact: matias@russitto.com
:Copyright: Este documento es de dominio público


.. *Realizado con Vim y rst2pdf.*

.. rst2pdf -s esfinge -v russitto.txt
.. vim:set ts=2 sw=2 ft=rst et:
